﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA_ASM1
{
    public class GiaoVienList : List<GiaoVien>
    {
        //Hien thi tat ca giao vien
        public void HienThiTatCa()
        {
            if(this.Count <= 0) {
                Console.WriteLine("Chua co giao vien nao trong danh sach");
                return;
            }
            Console.WriteLine("\n---Danh sach giao vien---");
            int count = 1;
            foreach(var giaoVien in this)
            {
                Console.Write(count++ + ". ");
                giaoVien.XuatThongTin();
            }
            Console.WriteLine("-------------------------");
        }

        //Hien thi giao vien luong thap nhat
        public void HienThiGiaoVienLuongThapNhat()
        {
            if(this.Count <= 0)
            {
                Console.WriteLine("Chua co giao vien nao trong danh sach");
                return;
            }
            List<GiaoVien> gvList = this.Where(o => o.TinhLuong() == this.MinBy(o => o.TinhLuong()).TinhLuong()).ToList();
            
            Console.WriteLine("\n---Giao vien luong thap nhat---");
            foreach(var giaoVien in gvList)
            {
                giaoVien.XuatThongTin();
            }
            Console.WriteLine("-------------------------");
        }


        //Nhap giao vien moi
        public GiaoVien NhapGiaoVien()
        {
            try
            {
                GiaoVien gv = new GiaoVien();
                gv.NhapThongTin();
                this.Add(gv);
                Console.WriteLine("\n---Nhap thong tin thanh cong!!!---");
                return gv;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                return null;
            }
        }

        public void XoaGiaoVien()
        {
            HienThiTatCa();
            if (this.Count <= 0) return;
            try
            {
                Console.Write("Nhap so thu tu giao vien ban muon xoa: ");
                int index = Int32.Parse(Console.ReadLine());
                this.ElementAt(index-1);
                Console.Write("Ban co chac chan muon xoa, Nhap (y/n): ");
                string input = Console.ReadLine();
                if(input == "y")
                {
                    this.RemoveAt(index - 1);
                    Console.WriteLine("\n---Xoa thong tin thanh cong!!!---");
                }
                else if(input == "n")
                {

                }
                else
                {
                    throw new Exception("Lua chon khong ton tai!!!");
                }
            }
            catch(FormatException e)
            {
                Console.WriteLine("So thu tu khong hop le!!!");
            }
            catch(ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("So thu tu khong ton tai!!!");
            }
        }

        public GiaoVien SuaGiaoVien()
        {
            HienThiTatCa();
            if (this.Count <= 0) return null;
            try
            {
                Console.Write("Nhap so thu tu giao vien ban muon sua: ");
                int index = Int32.Parse(Console.ReadLine());
                this.ElementAt(index-1);
                Console.Write("Ban co chac chan muon sua, Nhap (y/n): ");
                string input = Console.ReadLine();
                if (input == "y")
                {
                    GiaoVien gv = this.ElementAt(index - 1);
                    gv.NhapThongTin();
                    Console.WriteLine("\n---Sua thong tin thanh cong!!!---");
                    return gv;
                }
                else if (input == "n")
                {

                }
                else
                {
                    throw new Exception("Lua chon khong ton tai!!!");
                }
                return null;
            }
            catch (FormatException e)
            {
                Console.WriteLine("So thu tu khong hop le!!!");
                return null;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("So thu tu khong ton tai!!!");
                return null;
            }
        }
    }
}
