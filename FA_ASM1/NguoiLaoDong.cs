﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA_ASM1
{
    public class NguoiLaoDong
    {
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public decimal LuongCoBan { get; set; }

        public NguoiLaoDong()
        {

        }
        public NguoiLaoDong(string HoTen, int NamSinh, decimal LuongCoBan)
        {
            this.HoTen = HoTen;
            this.NamSinh = NamSinh;
            this.LuongCoBan = LuongCoBan;
        }
        public void NhapThongTin()
        {
            Console.Write("Nhap Ho Ten: ");
            this.HoTen = Console.ReadLine();
            if (string.IsNullOrEmpty(this.HoTen))
            {
                throw new Exception("Ho ten ko duoc de trong!!!");
            }
            Console.Write("Nhap nam sinh: ");
            try
            {
                this.NamSinh = Int32.Parse(Console.ReadLine());
            }
            catch(FormatException e)
            {
                throw new Exception("Nam sinh ko dung format!!!");
            }
            Console.Write("Nhap luong co ban: ");
            try
            {
                this.LuongCoBan = Decimal.Parse(Console.ReadLine());
            }
            catch (FormatException e)
            {
                throw new Exception("Luong co ban ko dung format!!!");
            }
        }
        public decimal TinhLuong() //trả về giá trị thuộc tính Lương cơ bản
        {
            return LuongCoBan;
        }
        public void XuatThongTin() //In ra câu “Ho ten la: {họ tên}, nam sinh: {năm sinh}, luong co ban: { lương cơ bản}.”
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
        }  


    }
}
