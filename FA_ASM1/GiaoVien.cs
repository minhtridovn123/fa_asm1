﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA_ASM1
{
    public class GiaoVien : NguoiLaoDong
    {
        public decimal HeSoLuong { get; set; }
        public GiaoVien()
        {
            
        }
        public GiaoVien(string HoTen, int NamSinh, decimal LuongCoBan, decimal HeSoLuong) : base(HoTen,NamSinh,LuongCoBan)
        { 
            this.HeSoLuong = HeSoLuong;
        }
        
        public void NhapThongTin()
        {
            base.NhapThongTin();
            Console.Write("Nhap he so luong:");
            try
            {
                this.HeSoLuong = Decimal.Parse(Console.ReadLine());
            }
            catch(FormatException e)
            {
                throw new Exception("He so luong khong dung format!!!");
            }
        }  
        public decimal TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25M;
        } //Trả về giá trị theo công thức: lương cơ bản* hệ số lương * 1.25
        public void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan.ToString("C2")}, " +
                $"he so luong: {HeSoLuong.ToString("P2")}, luong: {TinhLuong().ToString("C2")}.");
        }
        public decimal XuLy()
        {
            return HeSoLuong + 0.6M;
        } //thực hiện phép tính: hệ số lương + 0.6

    }
}
