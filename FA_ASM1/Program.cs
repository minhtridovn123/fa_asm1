﻿using System.Linq.Expressions;

namespace FA_ASM1
{
    public class Program
    {
        private static GiaoVienList GiaoVienList = new GiaoVienList
        {
            new GiaoVien{HoTen="Tri",NamSinh=2003,LuongCoBan=1000,HeSoLuong=0.5M },
            new GiaoVien{HoTen="Minh",NamSinh=2003,LuongCoBan=2000,HeSoLuong=0.1M },
            new GiaoVien{HoTen="Anh",NamSinh=2003,LuongCoBan=3000,HeSoLuong=0.3M },
        };
        public static void Main(string[] args)
        {
            Console.WriteLine("-----Quan ly thong tin Giao Vien-----");
            String[] options = {"1- Nhap Giao Vien",
                            "2- Hien thi tat ca",
                            "3- Hien thi Giao Vien luong thap nhat",
                            "4- Xoa giao vien",
                            "5- Sua giao vien",
                            "6- Clear man hinh",
                            "7- Thoat"
                                };
            int option = 1;
            while (option <= 6 && option >= 1)
            {
                try
                {
                    Menu.PrintMenu(options);
                    option = Int32.Parse(Console.ReadLine());
                    switch (option)
                    {
                        case 1:
                            {
                                GiaoVien gv = null;
                                while (gv == null)
                                {
                                    gv = GiaoVienList.NhapGiaoVien();
                                }
                                Console.ReadLine();
                                break;
                            }
                        case 2:
                            {
                                GiaoVienList.HienThiTatCa();
                                Console.ReadLine();
                                break;
                            }
                        case 3:
                            {
                                GiaoVienList.HienThiGiaoVienLuongThapNhat();
                                Console.ReadLine();
                                break;
                            }
                        case 4:
                            {
                                GiaoVienList.XoaGiaoVien();
                                Console.ReadLine();
                                break;
                            }
                        case 5:
                            {
                                GiaoVienList.SuaGiaoVien();
                                Console.ReadLine();
                                break;
                            }
                        case 6:
                            { 
                                Console.Clear();
                                Thread.Sleep(200);
                                Console.WriteLine("-----Quan ly thong tin Giao Vien-----");
                                break;
                            }
                        default:
                            {
                                break;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Khong co lua chon nay!!!");
                    Console.ReadLine();
                }
            }
        }
    }
}